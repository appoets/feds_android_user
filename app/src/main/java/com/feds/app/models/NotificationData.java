package com.feds.app.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationData implements Serializable {

    public List<CustomDatum> getCustomDataList() {
        return customDataList;
    }

    public void setCustomDataList(List<CustomDatum> customDataList) {
        this.customDataList = customDataList;
    }

    public CustomDatum getCustomData() {
        return customData;
    }

    public void setCustomData(CustomDatum customData) {
        this.customData = customData;
    }

    @SerializedName("custom data")
    @Expose
    private List<CustomDatum> customDataList = null;

    @SerializedName("custom data")
    @Expose
    private CustomDatum customData = null;



}
