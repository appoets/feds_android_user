package com.feds.app.models;

import com.google.gson.annotations.SerializedName;

public class DeliveryChargeResponse {
    @SerializedName("delivery_fee")
    double deliveryfee;
    @SerializedName("moms")
    double moms;
    @SerializedName("total_nf")
    double total;
    @SerializedName("moms_nf")
    double momsValue;

    public double getDeliveryfee() {
        return deliveryfee;
    }

    public void setDeliveryfee(double deliveryfee) {
        this.deliveryfee = deliveryfee;
    }

    public double getMoms() {
        return moms;
    }

    public void setMoms(double moms) {
        this.moms = moms;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getMomsValue() {
        return momsValue;
    }

    public void setMomsValue(double momsValue) {
        this.momsValue = momsValue;
    }


}
