package com.feds.app.fragments;

import java.util.List;

class JavaUtils {
    public static boolean isNullOrEmpty(List list) {
        return list == null || list.isEmpty();
    }
}
