package com.feds.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.feds.app.R;
import com.feds.app.activities.payment.PaymentWebViewActivity;
import com.feds.app.adapter.AccountPaymentAdapter;
import com.feds.app.build.api.ApiClient;
import com.feds.app.build.api.ApiInterface;
import com.feds.app.fragments.CartFragment;
import com.feds.app.helper.CustomDialog;
import com.feds.app.helper.GlobalData;
import com.feds.app.helper.SharedHelper;
import com.feds.app.models.Card;
import com.feds.app.models.Message;
import com.feds.app.models.Order;
import com.feds.app.models.User;
import com.google.firebase.iid.FirebaseInstanceId;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.feds.app.helper.GlobalData.cardArrayList;
import static com.feds.app.helper.GlobalData.currencySymbol;
import static com.feds.app.helper.GlobalData.isCardChecked;


public class AccountPaymentActivity extends AppCompatActivity implements PaymentResultListener {

    public static ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    public static CustomDialog customDialog;
    public static Context context;
    public static AccountPaymentAdapter accountPaymentAdapter;
    public static RelativeLayout cashPaymentLayout;
    public static RelativeLayout cardPaymentLayout;
    public static LinearLayout walletPaymentLayout;
    public static RadioButton cashCheckBox,cardRb;
    public static CheckBox acceptTerms;
    public static Button proceedToPayBtn;
    public static LinearLayout proceedLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.payment_method_lv)
    ListView paymentMethodLv;
    @BindView(R.id.wallet_amount_txt)
    TextView walletAmtTxt;
    @BindView(R.id.wallet_layout)
    RelativeLayout walletLayout;
    @BindView(R.id.payment_option)
    TextView paymentOptionTv;

    @BindView(R.id.add_new_cart)
    TextView addNewCart;


    String device_token, device_UDID,userAddressId;
    String TAG = "Login";
    boolean isWalletVisible = false;
    boolean isCashVisible = false;
    boolean isDibsPayment = false;
    boolean isProfile = false;
    double totalAmount = 0.0;

    Integer mEstimatedDeliveryTime = 0;
    String mRestaurantType = "";
    boolean mIsImmediate = false;


    Integer mAmount = 0;
    String mEmail = "", mMobile = "";

    public static void deleteCard(final int id) {
        customDialog.show();
        Call<Message> call = apiInterface.deleteCard(id);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(@NonNull Call<Message> call, @NonNull Response<Message> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    Toast.makeText(context, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < GlobalData.cardArrayList.size(); i++) {
                        if (GlobalData.cardArrayList.get(i).getId().equals(id)) {
                            GlobalData.cardArrayList.remove(i);
                            accountPaymentAdapter.notifyDataSetChanged();
                            return;
                        }
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("error"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Message> call, @NonNull Throwable t) {
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                customDialog.dismiss();
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_payment);
        ButterKnife.bind(this);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        context = AccountPaymentActivity.this;
        customDialog = new CustomDialog(context);
        cashPaymentLayout =  findViewById(R.id.cash_payment_layout);
        cardPaymentLayout =  findViewById(R.id.card_payment_layout);
        walletPaymentLayout = (LinearLayout) findViewById(R.id.wallet_payment_layout);
        proceedToPayBtn = (Button) findViewById(R.id.proceed_to_pay_btn);
        proceedLayout = (LinearLayout) findViewById(R.id.proceedLayout);
        cashCheckBox = (RadioButton) findViewById(R.id.cash_check_box);
        cardRb = (RadioButton) findViewById(R.id.dibs_payment_rb);
        acceptTerms = (CheckBox) findViewById(R.id.acceptTerms);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        isWalletVisible = getIntent().getBooleanExtra("is_show_wallet", false);
        isCashVisible = getIntent().getBooleanExtra("is_show_cash", false);
        isDibsPayment = getIntent().getBooleanExtra("is_show_dibspayment", false);
        userAddressId = getIntent().getStringExtra("useraddressId");
        totalAmount = getIntent().getDoubleExtra("totalamount",0.0);
        isProfile = getIntent().getBooleanExtra("fromProfile",false);

        cardArrayList = new ArrayList<>();
        accountPaymentAdapter = new AccountPaymentAdapter(AccountPaymentActivity.this, cardArrayList, !isCashVisible);
        paymentMethodLv.setAdapter(accountPaymentAdapter);

        if (isWalletVisible) {
            walletPaymentLayout.setVisibility(VISIBLE);
            proceedLayout.setVisibility(GONE);
            cardPaymentLayout.setVisibility(VISIBLE);

            cashPaymentLayout.setVisibility(GONE);

            paymentOptionTv.setVisibility(GONE);
        } else
            walletPaymentLayout.setVisibility(GONE);

        if (isCashVisible||isProfile)
            cashPaymentLayout.setVisibility(GONE);
        else
            cashPaymentLayout.setVisibility(GONE);
        if (isDibsPayment||isProfile)
            cardPaymentLayout.setVisibility(VISIBLE);
        else
            cardPaymentLayout.setVisibility(GONE);

        cashPaymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cashCheckBox.setChecked(true);
                cardRb.setChecked(false);
                isCardChecked = false;
                accountPaymentAdapter.notifyDataSetChanged();
                if(!isProfile)
                proceedLayout.setVisibility(VISIBLE);
                for (int i = 0; i < cardArrayList.size(); i++) {
                    cardArrayList.get(i).setChecked(false);
                }
                accountPaymentAdapter.notifyDataSetChanged();
            }
        });
        cardPaymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardRb.setChecked(true);
                cashCheckBox.setChecked(false);
                isCardChecked = true;
                accountPaymentAdapter.notifyDataSetChanged();
                if(!isProfile)
                    proceedLayout.setVisibility(VISIBLE);
                for (int i = 0; i < cardArrayList.size(); i++) {
                    cardArrayList.get(i).setChecked(false);
                }
                accountPaymentAdapter.notifyDataSetChanged();
            }
        });

        proceedToPayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (acceptTerms.isChecked()) {
                    if (isCardChecked) {
                        /*for (int i = 0; i < cardArrayList.size(); i++) {
                            if (cardArrayList.get(i).isChecked()) {
                                Card card = cardArrayList.get(i);
                                CartFragment.checkoutMap.put("payment_mode", "stripe");
                                CartFragment.checkoutMap.put("card_id", String.valueOf(card.getId()));
                                checkOut(CartFragment.checkoutMap);
                                return;
                            }
                        }*/

                        startActivityForResult(new Intent(AccountPaymentActivity.this, PaymentWebViewActivity.class).putExtra("amount",String.valueOf(totalAmount)).putExtra("type","order").putExtra("user_address_id",userAddressId),100);


                    } else if (cashCheckBox.isChecked()) {
                        CartFragment.checkoutMap.put("payment_mode", "cash");
                        checkOut(CartFragment.checkoutMap);
                    } else {
                        Toast.makeText(context, "Please select payment mode", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Please accept the terms & conditions", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100 &&resultCode==RESULT_OK)
        {
            if(data!=null)
            {
                Log.d("lastsegment",data.getStringExtra("paymentId"));
                CartFragment.checkoutMap.put("payment_mode", "easy");

                CartFragment.checkoutMap.put("paymentId", data.getStringExtra("paymentId"));

                onSuccessCardpayment(CartFragment.checkoutMap);
            }
        }
    }
    private void onSuccessCardpayment(HashMap<String, String> map) {
        customDialog.show();
        Call<Order> call = apiInterface.onCardPayment(map);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(@NonNull Call<Order> call, @NonNull Response<Order> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    GlobalData.addCart = null;
                    GlobalData.notificationCount = 0;
                    GlobalData.selectedShop = null;
                    GlobalData.profileModel.setWalletBalance(response.body().getUser().getWalletBalance());
                    GlobalData.isSelectedOrder = new Order();
                    GlobalData.isSelectedOrder = response.body();
                    if (mRestaurantType.equalsIgnoreCase("PICKUP")) {
                        if (mIsImmediate) {
                            showPickUpSuccessDialog();
                        } else {
                            startActivity(new Intent(context, CurrentOrderDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                        }
                    } else {
                        startActivity(new Intent(context, CurrentOrderDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                    startActivity(new Intent(context, CurrentOrderDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Order> call, @NonNull Throwable t) {
                Toast.makeText(AccountPaymentActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void checkOut(HashMap<String, String> map) {
        customDialog.show();
        Call<Order> call = apiInterface.postCheckout(map);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(@NonNull Call<Order> call, @NonNull Response<Order> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    GlobalData.addCart = null;
                    GlobalData.notificationCount = 0;
                    GlobalData.selectedShop = null;
                    GlobalData.profileModel.setWalletBalance(response.body().getUser().getWalletBalance());
                    GlobalData.isSelectedOrder = new Order();
                    GlobalData.isSelectedOrder = response.body();
                    if (mRestaurantType.equalsIgnoreCase("PICKUP")) {
                        if (mIsImmediate) {
                            showPickUpSuccessDialog();
                        } else {
                            startActivity(new Intent(context, CurrentOrderDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                        }
                    } else {
                        startActivity(new Intent(context, CurrentOrderDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                    startActivity(new Intent(context, CurrentOrderDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Order> call, @NonNull Throwable t) {
                Toast.makeText(AccountPaymentActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCardList() {
        customDialog.show();
        Call<List<Card>> call = apiInterface.getCardList();
        call.enqueue(new Callback<List<Card>>() {
            @Override
            public void onResponse(@NonNull Call<List<Card>> call, @NonNull Response<List<Card>> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    cardArrayList.clear();
                    cardArrayList.addAll(response.body());
                    accountPaymentAdapter.notifyDataSetChanged();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("error"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Card>> call, @NonNull Throwable t) {
                customDialog.dismiss();
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
    }


    protected void showDialog(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDeviceToken();
        getProfile();
        getCardList();
    }


    @OnClick({R.id.wallet_layout, R.id.add_new_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.wallet_layout:
                startActivity(new Intent(this, WalletActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                finish();
                break;
            case R.id.add_new_cart:
                startActivity(new Intent(AccountPaymentActivity.this, AddCardActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                break;
        }
    }

    private void showPickUpSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AccountPaymentActivity.this);
        final FrameLayout frameView = new FrameLayout(AccountPaymentActivity.this);
        builder.setView(frameView);
        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_order_pickup, frameView);
        alertDialog.setCancelable(false);
        Button mBtnOk = dialogView.findViewById(R.id.btn_ok);
        TextView mTxtTime =
                dialogView.findViewById(R.id.txt_time);
        mTxtTime.setText(mEstimatedDeliveryTime + " mins");
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(context, CurrentOrderDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        alertDialog.show();
    }


    public void initializePayment(Integer amount) {
        mEmail = GlobalData.profileModel.getEmail();
        mMobile = GlobalData.profileModel.getPhone();
        final Checkout mCheckout = new Checkout();
        mCheckout.setImage(R.mipmap.ic_launcher);
        final Activity activity = this;
        try {
            JSONObject options = new JSONObject();
            options.put("name", activity.getString(R.string.app_name));
            options.put("description", activity.getString(R.string.app_name));
//            options.put("order_id", "order_123456");
            options.put("currency", "INR");
            options.put("amount", amount);
            JSONObject preFill = new JSONObject();
            if (mEmail.equalsIgnoreCase("")) {
                preFill.put("email", "");
            } else {
                preFill.put("email", mEmail);
            }
            if (mMobile.equalsIgnoreCase("")) {
                preFill.put("contact", "");
            } else {
                preFill.put("contact", mMobile);
            }
            options.put("prefill", preFill);
            mCheckout.open(activity, options);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onPaymentSuccess(String s) {

        CartFragment.checkoutMap.put("payment_mode", "razorpay");
        CartFragment.checkoutMap.put("rzp_payment_id", s);
        checkOut(CartFragment.checkoutMap);

    }

    @Override
    public void onPaymentError(int i, String s) {

        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

    }


    private void getProfile() {
        HashMap<String, String> map = new HashMap<>();
        map.put("device_type", "android");
        map.put("device_id", device_UDID);
        map.put("device_token", device_token);
        Call<User> getprofile = apiInterface.getProfile(map);
        getprofile.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                customDialog.dismiss();
                GlobalData.profileModel = response.body();
                int walletMoney = GlobalData.profileModel.getWalletBalance();
                walletAmtTxt.setText(walletMoney + " " + currencySymbol);
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                customDialog.dismiss();
            }
        });
    }

    public void getDeviceToken() {
        try {
            if (!SharedHelper.getKey(context, "device_token").equals("") && SharedHelper.getKey(context, "device_token") != null) {
                device_token = SharedHelper.getKey(context, "device_token");
                Log.d(TAG, "GCM Registration Token: " + device_token);
            } else {
                device_token = "" + FirebaseInstanceId.getInstance().getToken();
                SharedHelper.putKey(context, "device_token", "" + FirebaseInstanceId.getInstance().getToken());
                Log.d(TAG, "Failed to complete token refresh: " + device_token);
            }
        } catch (Exception e) {
            device_token = "COULD NOT GET FCM TOKEN";
            Log.d(TAG, "Failed to complete token refresh");
        }

        try {
            device_UDID = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            Log.d(TAG, "Device UDID:" + device_UDID);
        } catch (Exception e) {
            device_UDID = "COULD NOT GET UDID";
            e.printStackTrace();
            Log.d(TAG, "Failed to complete device UDID");
        }
    }
}
