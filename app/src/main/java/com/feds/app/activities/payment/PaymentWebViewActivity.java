package com.feds.app.activities.payment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;


import com.feds.app.R;
import com.feds.app.helper.SharedHelper;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.List;

public class PaymentWebViewActivity extends AppCompatActivity {
    private static final String TAG = PaymentWebViewActivity.class.getSimpleName();
    private WebView paymentWebView;
    private boolean isPaymentSuccessful;
    ImageView backImg;
    String amount ="";
    String type="",Url="",userId,promocodeId="",wallet="";
    String postData="",userAddressid="",promoId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dibs_payment);
        backImg = findViewById(R.id.backArrow);
        paymentWebView = findViewById(R.id.webview);
        paymentWebView.getSettings().setJavaScriptEnabled(true);
        paymentWebView.getSettings().setDomStorageEnabled(true);
        paymentWebView.getSettings().setLoadWithOverviewMode(true);
        paymentWebView.getSettings().setUseWideViewPort(false);
        paymentWebView.getSettings().setBuiltInZoomControls(true);
        paymentWebView.getSettings().setDisplayZoomControls(false);
        paymentWebView.getSettings().setSupportZoom(true);
        paymentWebView.getSettings().setAllowFileAccess(true);
        paymentWebView.getSettings().setAllowContentAccess(true);
        paymentWebView.getSettings().setDefaultTextEncodingName("utf-8");
        paymentWebView.setWebChromeClient(new WebChromeClient());
        paymentWebView.setWebViewClient(new MyWebViewClient());
        paymentWebView.loadUrl(getIntent().getStringExtra("html"));
        amount=getIntent().getStringExtra("amount");
        type=getIntent().getStringExtra("type");
        userAddressid=getIntent().getStringExtra("user_address_id");
            userId=SharedHelper.getKey(PaymentWebViewActivity.this,"userId");
          promocodeId  =(SharedHelper.getKey(PaymentWebViewActivity.this,"promocode").equalsIgnoreCase(""))?SharedHelper.getKey(PaymentWebViewActivity.this,"promocode"):"";
            wallet=SharedHelper.getKey(PaymentWebViewActivity.this,"is_wallet").equalsIgnoreCase("1")?"1":"0";

        if(!promocodeId.equalsIgnoreCase("")&&!wallet.equalsIgnoreCase("0"))
        {
            promoId="&promocode_id="+promocodeId +"&wallet="+wallet ;
        }
        else if(!promocodeId.equalsIgnoreCase("")&&wallet.equalsIgnoreCase(""))
        {
            promoId="&promocode_id="+promocodeId;
        }
        else if(promocodeId.equalsIgnoreCase("")&&!wallet.equalsIgnoreCase("0"))
        {
            promoId="&wallet="+wallet;
        }
        else {
            promoId="";
        }

            if(!userAddressid.equalsIgnoreCase("null")&&userAddressid.equalsIgnoreCase(""))
        {
            if(promoId.equalsIgnoreCase(""))
            {
                postData = "amount=" + amount + "&type=" + type+"&user_id="+userId;
            }
            else
            {
                postData = "amount=" + amount + "&type=" + type+"&user_id="+userId+promoId;

            }

        }
        else
        {
            if(promoId.equalsIgnoreCase(""))
            {
                postData = "amount=" + amount + "&type=" + type+"&user_address_id="+userAddressid+"&user_id="+userId;

            }
            else
            {
                postData = "amount=" + amount + "&type=" + type+"&user_address_id="+userAddressid+"&user_id="+userId+promoId;

            }

        }


        Url="https://feds.se/api/user/payment/easy/checkout?"+postData;
        Log.d("url",Url);
        paymentWebView.loadUrl(Url);
      /*  backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/

    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d(TAG + "finish", url);

            if (url.startsWith("intent://")) {
                Log.d(TAG + "intent", url);

                try {
                    Context context = view.getContext();
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                    if (intent != null) {

                        PackageManager packageManager = context.getPackageManager();
                        ResolveInfo info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
                        if (info != null) {
                            context.startActivity(intent);
                        } else {
                            String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                            view.loadUrl(fallbackUrl);
                        }

                    }
                } catch (URISyntaxException e) {
                    Log.e(TAG, "Can't resolve intent://", e);

                }
            }

            else if (url.contains("https://feds.se/api/user/payment/easy/checkout?paymentId")) {
                Uri uri = Uri.parse(url);
                Log.d("lastsegment", uri.getQueryParameter("paymentId"));
                List<String> path = uri.getPathSegments();
                Intent intent = new Intent();
                intent.putExtra("paymentId", uri.getQueryParameter("paymentId"));
//                intent.putExtra("paymentId", path.get(path.size() - 1));
                setResult(RESULT_OK, intent);
                finish();

            }
        }


        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Uri url = Uri.parse(request.getUrl().toString());
            Log.d(TAG + "should", url.toString());
            if (url.toString().startsWith("intent://")) {
                try {
                    Context context = view.getContext();
                    Intent intent = Intent.parseUri(String.valueOf(url), Intent.URI_INTENT_SCHEME);

                    if (intent != null) {

                        PackageManager packageManager = context.getPackageManager();
                        ResolveInfo info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
                        if (info != null) {
                            context.startActivity(intent);
                        } else {
                            String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                            view.loadUrl(fallbackUrl);
                        }

                        return true;
                    }
                } catch (URISyntaxException e) {
                    Log.e(TAG, "Can't resolve intent://", e);

                }
            }
            else if (request.getUrl().toString().contains("https://feds.se/api/user/payment/easy/checkout?paymentId")) {
                Uri uri = Uri.parse(request.getUrl().toString());
                List<String> path = uri.getPathSegments();
                Log.d("lastsegment", uri.getQueryParameter("paymentId"));


                Intent intent = new Intent();
//                intent.putExtra("paymentId", path.get(path.size() - 1));
                intent.putExtra("paymentId", uri.getQueryParameter("paymentId"));
                setResult(RESULT_OK, intent);
                finish();

            }

            return false;

        }
    }
}

