package com.feds.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.feds.app.activities.payment.PaymentWebViewActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.feds.app.R;
import com.feds.app.adapter.AccountPaymentAdapter;
import com.feds.app.build.api.ApiClient;
import com.feds.app.build.api.ApiInterface;
import com.feds.app.helper.GlobalData;
import com.feds.app.helper.CustomDialog;
import com.feds.app.helper.SharedHelper;
import com.feds.app.models.AddMoney;
import com.feds.app.models.Card;
import com.feds.app.models.Message;
import com.feds.app.models.User;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.feds.app.helper.GlobalData.cardArrayList;

public class AddMoneyActivity extends AppCompatActivity implements PaymentResultListener {

    public static final String TAG = "AddMoneyActivity";

    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    Context context = AddMoneyActivity.this;
    CustomDialog customDialog;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.promo_layout)
    RelativeLayout promoLayout;
    @BindView(R.id.payment_method_lv)
    ListView paymentMethodLv;

    @BindView(R.id.amount_txt)
    EditText amountTxt;
    @BindView(R.id.pay_btn)
    Button payBtn;
    String device_token, device_UDID;

    public static AccountPaymentAdapter accountPaymentAdapter;
    Integer mAmount = 0;
    String mEmail = "", mMobile = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_amount);
        ButterKnife.bind(this);
        Checkout.preload(this);

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        customDialog = new CustomDialog(context);
        title.setText(context.getResources().getString(R.string.add_money));
        cardArrayList = new ArrayList<>();
        accountPaymentAdapter = new AccountPaymentAdapter(context, cardArrayList, false);
        paymentMethodLv.setAdapter(accountPaymentAdapter);
        amountTxt.setHint(GlobalData.currencySymbol);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCardList();
    }

    //TODO update getCards API and the recyclerview
    private void getCardList() {
        customDialog.show();
        Call<List<Card>> call = apiInterface.getCardList();
        call.enqueue(new Callback<List<Card>>() {
            @Override
            public void onResponse(@NonNull Call<List<Card>> call, @NonNull Response<List<Card>> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    cardArrayList.clear();
                    cardArrayList.addAll(response.body());
                    accountPaymentAdapter.notifyDataSetChanged();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("error"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Card>> call, @NonNull Throwable t) {
                customDialog.dismiss();
                Toast.makeText(AddMoneyActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, WalletActivity.class));
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
        finish();
    }



    @OnClick({R.id.back, R.id.promo_layout, R.id.pay_btn, R.id.lnrRazorpay, R.id.lblRazorpay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;

            case R.id.pay_btn:
                String amount = amountTxt.getText().toString();

                /*if (!isCardChecked) {
                    Toast.makeText(context, "Please choose your card", Toast.LENGTH_SHORT).show();
                } else if (amount.equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter amount", Toast.LENGTH_SHORT).show();
                } else if (Integer.parseInt(amount) == 0) {
                    Toast.makeText(context, "Please enter valid amount", Toast.LENGTH_SHORT).show();
                } else if (isCardChecked) {
                    for (int i = 0; i < cardArrayList.size(); i++) {
                        if (cardArrayList.get(i).isChecked()) {
                            Card card = cardArrayList.get(i);
                            HashMap<String, String> map = new HashMap<>();
                            map.put("amount", "" + amountTxt.getText().toString());
                            map.put("card_id", card.getId().toString());
                            addMoney(map);
                            return;
                        }
                    }
                } else {
                    Toast.makeText(context, "Please select your card", Toast.LENGTH_SHORT).show();
                }*/

                if(amountTxt.getText().toString().isEmpty())
                {
                    Toast.makeText(context, getString(R.string.pls_enter_amount), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    startActivityForResult(new Intent(AddMoneyActivity.this, PaymentWebViewActivity.class).putExtra("amount",amountTxt.getText().toString().trim()).putExtra("type","wallet").putExtra("user_address_id",""),100);
                }


                break;
            case R.id.promo_layout:
                startActivity(new Intent(this, PromotionActivity.class).putExtra("tag", TAG));
                overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100 &&resultCode==RESULT_OK)
        {
            if(data!=null)
            {
                Log.d("lastsegment",data.getStringExtra("paymentId"));

                addMoney(data.getStringExtra("paymentId"));
            }
        }
    }

    public void alertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton(getResources().getString(R.string.okay), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(context, AccountPaymentActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                        finish();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(ContextCompat.getColor(context, R.color.theme));
        pbutton.setTypeface(pbutton.getTypeface(), Typeface.BOLD);
    }



    private void addMoney(String paymentId) {
        customDialog.show();
        Call<User> call = apiInterface.OnWalletPayment(paymentId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
//                    Toast.makeText(AddMoneyActivity.this, context.getResources().getString(R.string.) , Toast.LENGTH_SHORT).show();
                    GlobalData.profileModel.setWalletBalance(response.body().getWalletBalance());
                    onBackPressed();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("card_id"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                customDialog.dismiss();
                Toast.makeText(AddMoneyActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void initializePayment(Integer amount) {
        mEmail = GlobalData.profileModel.getEmail();
        mMobile = GlobalData.profileModel.getPhone();
        final Checkout mCheckout = new Checkout();
        mCheckout.setImage(R.mipmap.ic_launcher);
        final Activity activity = this;
        try {
            JSONObject options = new JSONObject();
            options.put("name", activity.getString(R.string.app_name));
            options.put("description", activity.getString(R.string.app_name));
//            options.put("order_id", "order_123456");
            options.put("currency", "INR");
            options.put("amount", amount);
            JSONObject preFill = new JSONObject();
            if (mEmail.equalsIgnoreCase("")) {
                preFill.put("email", "");
            } else {
                preFill.put("email", mEmail);
            }
            if (mMobile.equalsIgnoreCase("")) {
                preFill.put("contact", "");
            } else {
                preFill.put("contact", mMobile);
            }
            options.put("prefill", preFill);
            mCheckout.open(activity, options);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPaymentSuccess(String s) {

        addRazorPayMoney(s);

    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    private void addRazorPayMoney(String payId) {
        customDialog.show();
        Call<Message> call = apiInterface.addRazorWallet(payId);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(@NonNull Call<Message> call, @NonNull Response<Message> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    customDialog.dismiss();
//                    Toast.makeText(AddMoneyActivity.this, context.getResources().getString(R.string.) , Toast.LENGTH_SHORT).show();
//                    GlobalData.profileModel.setWalletBalance(response.body().getWalletBalance());
                    getDeviceToken();
                    getProfile();

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("card_id"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Message> call, @NonNull Throwable t) {
                customDialog.dismiss();
                Toast.makeText(AddMoneyActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void getProfile() {
        HashMap<String, String> map = new HashMap<>();
        map.put("device_type", "android");
        map.put("device_id", device_UDID);
        map.put("device_token", device_token);
        Call<User> getprofile = apiInterface.getProfile(map);
        getprofile.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                customDialog.dismiss();
                GlobalData.profileModel = response.body();
                onBackPressed();

            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                customDialog.dismiss();
            }
        });
    }

    public void getDeviceToken() {
        try {
            if (!SharedHelper.getKey(context, "device_token").equals("") && SharedHelper.getKey(context, "device_token") != null) {
                device_token = SharedHelper.getKey(context, "device_token");
                Log.d(TAG, "GCM Registration Token: " + device_token);
            } else {
                device_token = "" + FirebaseInstanceId.getInstance().getToken();
                SharedHelper.putKey(context, "device_token", "" + FirebaseInstanceId.getInstance().getToken());
                Log.d(TAG, "Failed to complete token refresh: " + device_token);
            }
        } catch (Exception e) {
            device_token = "COULD NOT GET FCM TOKEN";
            Log.d(TAG, "Failed to complete token refresh");
        }

        try {
            device_UDID = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            Log.d(TAG, "Device UDID:" + device_UDID);
        } catch (Exception e) {
            device_UDID = "COULD NOT GET UDID";
            e.printStackTrace();
            Log.d(TAG, "Failed to complete device UDID");
        }
    }
}
